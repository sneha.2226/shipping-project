const controller = {};
const mysql = require("mysql");
var conn = mysql.createConnection({
  host     : 'localhost',
  user     : 'root',
  password : '',
  database : 'shipping'
});

controller.list = (req, res) => {
  conn.query('SELECT * FROM staff', (err, staffs) => {
    if (err) {
     res.json(err);
    }
    res.render('dashboard', {
       data: staffs
    });
   });
};

controller.add = (req, res) => {
  res.render("add");
};

controller.save = (req, res) => {
  const data = req.body;
  console.log(req.body)
  conn.query('INSERT INTO staff set ?', data, (err, staff) => {
    console.log(staff)
    res.redirect('/staff');
  })
};

controller.edit = (req, res) => {
  const { id } = req.params;
  conn.query("SELECT * FROM staff WHERE id = ?", [id], (err, rows) => {
    res.render('updatestaff', {
      data: rows[0]
    })
  });
};

controller.update = (req, res) => {
  const { id } = req.params;
  const newCustomer = req.body;
  conn.query('UPDATE staff set ? where id = ?', [newCustomer, id], (err, rows) => {
    res.redirect('/staff');
  });
};

controller.delete = (req, res) => {
  const { id } = req.params;
  conn.query('DELETE FROM staff WHERE id = ?', [id], (err, rows) => {
    res.redirect('/staff');
  });
}

module.exports = controller;
