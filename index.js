const express = require("express");
const path = require("path");
const PORT = 4000;

const app = express();



// importing routes
const staff = require("./routes/staff");

// settings
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "ejs");

app.use(express.urlencoded({ extended: false }));

// routes
app.use("/staff", staff);

// static files
app.use(express.static(path.join(__dirname, "public")));

app.get("/", (req, res) => {
  res.render("login");
});

app.post("/login", (req, res) => {
  try {
    var username = req.body.username;
    var pass = req.body.password;
    console.log("dshj", username, pass);
    if (username && pass) {
        if (username == "admin" && pass == "hello123") {
            res.redirect("/staff");
        } else {
            res.render("login", { err: { status: false, msg: "Invalid Credentials!" } });
        }
    } else {
      res.render("login", { err: { status: false, msg: "Missing Fields!" } });
    }
  } catch (error) {}
});

// starting the server
app.listen(PORT, () => {
  console.log(`server on port ${PORT}`);
});
