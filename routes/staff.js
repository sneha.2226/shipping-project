const router = require('express').Router();

const staff = require('../controllers/staffcontroller');

router.get('/', staff.list);
router.get('/add', staff.add);
router.post('/save', staff.save);
router.get('/update/:id', staff.edit);
router.post('/update/:id', staff.update);
router.get('/delete/:id', staff.delete);

module.exports = router;

